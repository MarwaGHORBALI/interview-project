import { Component, Input, OnInit } from '@angular/core';
import { Steps } from '@shared/models/project-information';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
  @Input() stepName !: string;

  navBarList: Array<{ titre: string, description: string, status: boolean, stepName: string }> = [
    {
      titre: 'Vos information',
      description: 'Les renseignements nécessaires au calcul de vos aides.',
      status: false,
      stepName: Steps.STEPONE
    },

    {
      titre: 'Votre projet',
      description: 'Les travaux que vous souhaitez réaliser.',
      status: false,
      stepName: Steps.STEPTWO
    },
    {
      titre: 'Résultats',
      description: '',
      status: false,
      stepName: Steps.STEPTHREE
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges() {
    this.navBarList.map(el => el.stepName == this.stepName ? el.status = true : el.status = false)
  }

}
