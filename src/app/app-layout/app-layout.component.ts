import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { PageNonElegibleComponent } from '@shared/components/page-non-elegible/page-non-elegible.component';
import { NextSteps, Steps } from '@shared/models/project-information';
import { MessageService } from '@shared/services/utilities/message.service';


@Component({
  selector: 'app-app-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.scss']
})
export class AppLayoutComponent implements OnInit, OnDestroy {
  formValidate !: boolean;
  stepName: string = Steps.STEPONE;
  subStepName: string  = 'STEPONE_1';
  locationPath!: string;
  private readonly subscriptions = new Subscription();
  constructor(private messageService: MessageService, private cdref: ChangeDetectorRef,
    private router: Router) { }

  ngOnInit(): void {
    const subscription = this.messageService.getMessage()
      .subscribe(data => {
        this.formValidate = data.formValidate;
        (data.step ? this.stepName = data.step : this.stepName = data.nextStepName);
        (data.subStepName ? this.subStepName = data.subStepName : null)
      })
    this.subscriptions.add(subscription);
  }

  onActivate(component: PageNonElegibleComponent) {
    if (component instanceof PageNonElegibleComponent) {
      this.locationPath = this.router.url;
    }
  }

  displayStep($event: NextSteps) {
    this.stepName = $event.nextStepName;
    this.subStepName = $event.subStepName;
    this.messageService.clearMessage();
    this.messageService.sendMessage($event);
  }

  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
