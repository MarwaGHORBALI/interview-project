import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { PrimeNgModule } from './primeng.module';
import { ButtonComponent } from './shared/components/button/button.component';
import { AppLayoutComponent } from './app-layout/app-layout.component';
import { HeaderComponent } from './app-layout/header/header.component';
import { SideBarComponent } from './app-layout/side-bar/side-bar.component';
import { PageNonElegibleComponent } from './shared/components/page-non-elegible/page-non-elegible.component';

@NgModule({
  declarations: [
    AppComponent,
    ButtonComponent,
    AppLayoutComponent,
    HeaderComponent,
    SideBarComponent,
    PageNonElegibleComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    PrimeNgModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
