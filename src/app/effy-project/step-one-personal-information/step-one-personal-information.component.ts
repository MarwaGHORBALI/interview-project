import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { animate, style, transition, trigger } from "@angular/animations";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { ProjectInformation, Steps } from "@shared/models/project-information"
import { MessageService } from '@shared/services/utilities/message.service';


@Component({
  selector: 'step-one-personal-information',
  templateUrl: './step-one-personal-information.component.html',
  styleUrls: ['./step-one-personal-information.component.scss'],
  animations: [
    trigger("inOutPaneAnimation", [
      transition(":enter", [
        style({ opacity: 0, transform: "translateX(100%)" }), 
        animate(
          "250ms ease-in-out",
          style({ opacity: 1, transform: "translateX(0)" })
        )
      ]),
      transition(":leave", [
        animate(
          "150ms ease-in-out",
          style({ opacity: 0, transform: "translateX(100%)" })
        )
      ])
    ])
  ]
})
export class StepOnePersonalInformationComponent implements OnInit, OnDestroy {
  personalInfoForm!: FormGroup;
  @Input() display !: boolean ;
  @Input() stepName !: string;
  @Input() stepOneValue !:ProjectInformation
  @Output() validateForm: EventEmitter<{ stepName: string, subStepName: string, validateForm: boolean ,stepOneValue :ProjectInformation}> = new EventEmitter<{ stepName: string, subStepName: string, validateForm: boolean ,stepOneValue :ProjectInformation }>();
  private readonly subscriptions = new Subscription();

  constructor(private formBuilder: FormBuilder, private messageService: MessageService) { }

  ngOnInit(): void {
    this.initInfoForm();
    const subscription = this.messageService.getMessage()
      .subscribe(data => {
        if (data.nextStepName == Steps.STEPONE) {
          switch (data.subStepName) {
            case 'STEPONE_1':
              this.display = false;
              break;
            case 'STEPONE_2':
              this.display = true;
              break;
            default:
              this.stepName = Steps.STEPTWO;
          }
        }
      })
      if(this.stepOneValue) this.getInfoForm();
    this.subscriptions.add(subscription);
  }

  initInfoForm(): void {
    this.personalInfoForm = this.formBuilder.group({
      firstName: ["", Validators.required],
      lastName: ["", Validators.required],
      civility: ["", Validators.required],
      email: ["", [Validators.required, Validators.email]],
      tel: ["", [Validators.required,Validators.pattern('(0|\\+33|0033)[1-9][0-9]{8}')]],
    });
  }

  getInfoForm(){
    this.personalInfoForm.setValue(this.stepOneValue);
  }

  // convenience getter for easy access to form fields
  get myPersonalInfoForm() {
    (!this.display && this.personalInfoForm.controls['firstName'].status == 'VALID'
      && this.personalInfoForm.controls['lastName'].status == 'VALID'
      && this.personalInfoForm.controls['civility'].status == 'VALID' ?
      this.validateForm.emit({ stepName: Steps.STEPONE, subStepName: 'STEPONE_1', validateForm: true ,stepOneValue: this.personalInfoForm.value}) :

      (this.display && this.personalInfoForm.controls['email'].status == 'VALID'
        && this.personalInfoForm.controls['tel'].status == 'VALID') ?
        this.validateForm.emit({ stepName: Steps.STEPONE, subStepName: 'STEPONE_2', validateForm: true , stepOneValue: this.personalInfoForm.value}) : '')
    return this.personalInfoForm.controls;
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

}
