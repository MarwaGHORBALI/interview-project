import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StepOnePersonalInformationComponent } from './step-one-personal-information.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('StepOnePersonalInformationComponent', () => {
  let component: StepOnePersonalInformationComponent;
  let fixture: ComponentFixture<StepOnePersonalInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StepOnePersonalInformationComponent ],
      imports :[FormsModule ,ReactiveFormsModule,BrowserAnimationsModule]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StepOnePersonalInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
