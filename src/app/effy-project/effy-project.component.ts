import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectInformation, Steps } from '@shared/models/project-information';
import { EffyProjectOperationService } from '@shared/services/effyProject/effy-project-operation.service';
import { MessageService } from '@shared/services/utilities/message.service';
import { Observable, Subscription, flatMap, mergeMap } from 'rxjs';

@Component({
  selector: 'app-effy-project',
  templateUrl: './effy-project.component.html',
  styleUrls: ['./effy-project.component.scss']
})
export class EffyProjectComponent implements OnInit, OnDestroy {
  stepName: string = Steps.STEPONE;
  formValidate !: boolean;
  stepOneValue !: ProjectInformation;
  stepTwoValue !: ProjectInformation;
  stepValue!: ProjectInformation;
  calculateAmountOfAid !: number | string;
  display: boolean = false;
  displayStepOne: boolean = true;
  displayStepTwo: boolean = false;
  displayStepThree: boolean = false;
  displayStepFour: boolean = false;
  private readonly subscriptions = new Subscription();
  constructor(private messageService: MessageService,
    private effyProjectOperationService: EffyProjectOperationService,
    private router: Router) { }

  ngOnInit(): void {
    const subscription = this.messageService.getMessage()
      .subscribe(data => {
        (data.step ? this.stepName = data.step : this.stepName = data.nextStepName);

        switch (data.subStepName) {
          case "STEPTWO_1":
            this.displayStepOne = true;
            this.displayStepTwo = false
            this.displayStepFour = false;
            this.displayStepThree = false
            break;
            case "STEPTWO_2":
              this.displayStepOne = false;
              this.displayStepTwo = true;
              this.displayStepThree = false
              this.displayStepFour = false;
            break;
            case "STEPTWO_3":
              console.log("dlkclsdnk")
              this.displayStepOne = false;
              this.displayStepTwo = false;
              this.displayStepThree = true
              this.displayStepFour = false;
            break;
            default:
              console.info('error back step');
        }
        if (data.subStepName == "STEPONE_2") this.display = true;

        if (this.stepName === Steps.STEPTHREE) {
          const { surface, revenu, personalNbr } = this.stepValue;
          this.costProject(surface, Number(revenu.replace(",", "")), personalNbr);
        }
      })
    this.subscriptions.add(subscription);
  }

  verifyForm($event: any) {
    this.stepName = $event.stepName;
    this.messageService.sendMessage({ step: $event.stepName, formValidate: this.formValidate, subStepName: $event.subStepName });
    this.formValidate = $event.validateForm;
    ($event.stepOneValue ? this.stepOneValue = $event.stepOneValue : '');
    ($event.stepTwoValue ? this.stepTwoValue = $event.stepTwoValue : '');
    this.stepValue = Object.assign(this.stepOneValue, this.stepTwoValue);
  }

  costProject(surface: number, revenu: number, nbrOfPerson: number) {
    this.effyProjectOperationService.costProject(surface).pipe(
      mergeMap((costProject) => this.effyProjectOperationService.amountOfAid(costProject, revenu, nbrOfPerson)))
      .subscribe(data => {
        this.calculateAmountOfAid = Math.ceil(data * 100) / 100;
        (Math.sign(data) !== -1 ? this.calculateAmountOfAid = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(this.calculateAmountOfAid) : this.router.navigateByUrl('/project-effy-non-eligible'));
      })
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
