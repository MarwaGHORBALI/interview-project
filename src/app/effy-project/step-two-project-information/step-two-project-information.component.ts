import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, LOCALE_ID, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProjectInformation, Steps } from '@shared/models/project-information';
import { MessageService } from '@shared/services/utilities/message.service';
import { Subscription } from 'rxjs';
import { CurrencyPipe } from '@angular/common';
import { animate, style, transition, trigger } from "@angular/animations";

@Component({
  selector: 'step-two-project-information',
  templateUrl: './step-two-project-information.component.html',
  styleUrls: ['./step-two-project-information.component.scss'],
  animations: [
    trigger("inOutAnimation", [
      transition(":enter", [
        style({ opacity: 0, transform: "translateX(100%)" }),
        animate(
          "250ms ease-in-out",
          style({ opacity: 1, transform: "translateX(0)" })
        )
      ]),
      transition(":leave", [
        animate(
          "150ms ease-in-out",
          style({ opacity: 0, transform: "translateX(100%)" })
        )
      ])
    ])
  ]
})

export class StepTwoProjectInformationComponent implements OnInit, OnDestroy {
  projectInfoForm!: FormGroup;
  submitted!: boolean;
  inputnumber = 1;
  @Input() displayStepOne !: boolean;
  @Input() displayStepTwo !: boolean ;
  @Input() displayStepThree !: boolean ;
  @Input() displayStepFour !: boolean ;
  @Input() stepName !: string;
  private  subscriptions = new Subscription();
  @Output() validateForm: EventEmitter<{ stepName: string, subStepName: string, validateForm: boolean, stepTwoValue: ProjectInformation }> = new EventEmitter<{ stepName: string, subStepName: string, validateForm: boolean, stepTwoValue: ProjectInformation }>();
  constructor(private formBuilder: FormBuilder, private messageService: MessageService, @Inject(LOCALE_ID) private locale: string) { }

  ngOnInit(): void {
    this.initProjectInfoForm();
    const subscription = this.messageService.getMessage()
      .subscribe(data => {
        if (data.nextStepName === Steps.STEPTWO) {
          switch (data.subStepName) {
           /*  case 'STEPTWO_1':
              this.displayStepOne = false;
              break;
            case 'STEPTWO_2':
              this.displayStepOne = false;
              this.displayStepTwo = true;
              break; */
            case 'STEPTWO_3':
              this.displayStepTwo = false;
              this.displayStepThree = true;
              this.displayStepFour = false;
              break;
            case 'STEPTWO_4':
              this.displayStepThree = false;
              this.displayStepFour = true;
              break;
            case Steps.NONELEGIBLE:
              this.displayStepOne = false;
              break;
            default:
              this.stepName = Steps.STEPTHREE;
          }
        }
      })
    this.subscriptions.add(subscription);
  }

  initProjectInfoForm(): void {
    this.projectInfoForm = this.formBuilder.group({
      personalNbr: [1, Validators.required],
      situation: ["", Validators.required],
      surface: [null, Validators.required],
      revenu: ["", Validators.required],
    });
  }

  // convenience getter for easy access to form fields
  get myProjectInfoForm() {
    if (this.projectInfoForm.value.situation == 'locataire') {
      this.validateForm.emit({ stepName: Steps.STEPTWO, subStepName: Steps.NONELEGIBLE, validateForm: true, stepTwoValue: this.projectInfoForm.value })
    } else {
      (this.displayStepOne && this.projectInfoForm.controls['situation'].status == 'VALID' ?
        this.validateForm.emit({ stepName: Steps.STEPTWO, subStepName: 'STEPTWO_1', validateForm: true, stepTwoValue: this.projectInfoForm.value }) :
        this.displayStepTwo && this.projectInfoForm.controls['personalNbr'].status == 'VALID' ? this.validateForm.emit({ stepName: Steps.STEPTWO, subStepName: 'STEPTWO_2', validateForm: true, stepTwoValue: this.projectInfoForm.value })
          : this.displayStepThree && this.projectInfoForm.controls['surface'].status == 'VALID' ? this.validateForm.emit({ stepName: Steps.STEPTWO, subStepName: 'STEPTWO_3', validateForm: true, stepTwoValue: this.projectInfoForm.value })
            : this.submitted === false && this.displayStepFour && this.projectInfoForm.controls['revenu'].status == 'VALID' ? this.validateForm.emit({ stepName: Steps.STEPTWO, subStepName: 'STEPTWO_4', validateForm: true, stepTwoValue: this.projectInfoForm.value }) : '')
    }
    return this.projectInfoForm.controls;
  }

  plusNbrOfPerson() {
    let nbrOfPerson = this.projectInfoForm.value.personalNbr + 1;
    this.projectInfoForm.controls['personalNbr'].setValue(nbrOfPerson);
  }
  minusNbrOfPerson() {
    if ((this.projectInfoForm.value.personalNbr != 1)) {
      let nbrOfPerson = this.projectInfoForm.value.personalNbr - 1;
      this.projectInfoForm.controls['personalNbr'].setValue(nbrOfPerson);;
    }

  }

  changeRevenuValue($event: any): void {
    const lowRevenu = 10000;
    const highRevenu = 100000;
    let revenu = ($event.target.value);
    if (revenu >= lowRevenu && revenu <= highRevenu) {
      this.projectInfoForm.controls['revenu'].setValue(this.formatCurrency($event.target.value));
      this.submitted = false;
    } else {
      this.submitted = true;
    }
  }

  formatCurrency(value: number): string {
    const formattedValue = new CurrencyPipe(this.locale).transform(value, 'EUR', 'symbol', '1.2-2');
    if (formattedValue !== null) {
      return formattedValue.replace('€', '');
    }
    return 'N/A';
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

}

