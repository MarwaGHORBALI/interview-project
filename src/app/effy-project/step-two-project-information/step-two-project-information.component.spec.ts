import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StepTwoProjectInformationComponent } from './step-two-project-information.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('StepTwoProjectInformationComponent', () => {
  let component: StepTwoProjectInformationComponent;
  let fixture: ComponentFixture<StepTwoProjectInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StepTwoProjectInformationComponent ],
      imports :[FormsModule ,ReactiveFormsModule,BrowserAnimationsModule]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StepTwoProjectInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
