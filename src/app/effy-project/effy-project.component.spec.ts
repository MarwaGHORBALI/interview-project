import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EffyProjectComponent } from './effy-project.component';
import { StepTwoProjectInformationComponent } from './step-two-project-information/step-two-project-information.component';
import { StepOnePersonalInformationComponent } from './step-one-personal-information/step-one-personal-information.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EffyProjectOperationService } from '@shared/services/effyProject/effy-project-operation.service';
import { Router } from '@angular/router';
import { of } from 'rxjs';

describe('EffyProjectComponent', () => {
  let component: EffyProjectComponent;
  let fixture: ComponentFixture<EffyProjectComponent>;
  let effyProjectOperationService: EffyProjectOperationService;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EffyProjectComponent, StepTwoProjectInformationComponent, StepOnePersonalInformationComponent],
      imports: [FormsModule, ReactiveFormsModule, BrowserAnimationsModule],
      providers: [
        { provide: EffyProjectOperationService, useValue: mockEffyProjectOperationService },
        { provide: Router, useValue: mockRouter }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();

    fixture = TestBed.createComponent(EffyProjectComponent);
    component = fixture.componentInstance;
    effyProjectOperationService = TestBed.inject(EffyProjectOperationService);
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should calculate the cost of the project', (done) => {
    const surface = 25;
    const revenu = 10000;
    const nbrOfPerson = 2;
    const costProject = 2000;
    const amountOfAid = 750;
    const amountOfAid2 = '750,00 €';

    spyOn(effyProjectOperationService, 'costProject').and.returnValue(of(costProject));
    spyOn(effyProjectOperationService, 'amountOfAid').and.returnValue(of(amountOfAid));
    spyOn(router, 'navigateByUrl');

    component.costProject(surface, revenu, nbrOfPerson);

    setTimeout(() => {
      expect(effyProjectOperationService.costProject).toHaveBeenCalledWith(surface);
      expect(effyProjectOperationService.amountOfAid).toHaveBeenCalledWith(costProject, revenu, nbrOfPerson);
      expect(component.calculateAmountOfAid).toBe(amountOfAid2);
      expect(router.navigateByUrl).not.toHaveBeenCalled();
      done();
    }, 0);
  });
});


const mockEffyProjectOperationService = {
  costProject: (surface: number) => { },
  amountOfAid: (costProject: number, revenu: number, nbrOfPerson: number) => { }
};

const mockRouter = {
  navigateByUrl: (url: string) => { }
};

