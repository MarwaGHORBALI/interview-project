import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { StepOnePersonalInformationComponent } from "./step-one-personal-information/step-one-personal-information.component";
import { StepTwoProjectInformationComponent } from "./step-two-project-information/step-two-project-information.component";
import { EffyProjectComponent } from "./effy-project.component";
import { RouterModule } from "@angular/router";
import { effyProjectRoutes } from "./effy-project.routing";






@NgModule({
  declarations: [
    EffyProjectComponent,
    StepOnePersonalInformationComponent,
    StepTwoProjectInformationComponent,
   
  ],

  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(effyProjectRoutes),
  ],
})
export class effyProjectModule {}
