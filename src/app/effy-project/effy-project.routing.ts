import { Routes } from "@angular/router";

//Components
import { EffyProjectComponent } from "./effy-project.component";

export const effyProjectRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "",
        component: EffyProjectComponent,
      },
    ],
  },
];
