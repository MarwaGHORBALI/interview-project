import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { ButtonComponent } from '@shared/components/button/button.component';
import { AppLayoutComponent } from './app-layout/app-layout.component';
import { HeaderComponent } from './app-layout/header/header.component';
import { SideBarComponent } from './app-layout/side-bar/side-bar.component';
import { PageNonElegibleComponent } from '@shared/components/page-non-elegible/page-non-elegible.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>
  let app: AppComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent
        
      ],

      imports: [RouterModule],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
  });

  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it(`should have as title 'kata-Effy'`, () => {
   // expect(app.title).toEqual('kata-Effy');
  });

  it('should render title', () => {
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
  //  expect(compiled.querySelector('.content span')?.textContent).toContain('kata-Effy app is running!');
  });
});
