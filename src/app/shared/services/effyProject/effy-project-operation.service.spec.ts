import { TestBed } from '@angular/core/testing';

import { EffyProjectOperationService } from './effy-project-operation.service';

describe('EffyProjectOperationService', () => {
  let service: EffyProjectOperationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EffyProjectOperationService);
  });

  it('should be created', () => {
    const service: EffyProjectOperationService = TestBed.get(EffyProjectOperationService);
    expect(service).toBeTruthy();
  });

  it('should calculate the cost of the project', () => {
    const surface = 100;
    const expectedResult = 8000;
    service.costProject(surface).subscribe(result => {
      expect(result).toBe(expectedResult);
    });
  });

  it("should calculate the amount of aid of the Project", () => {

    const costProject = 2000;
    const revenu = 10000;
    const nbrOfPerson = 2;
    const expectedAmount = 750;

    service.amountOfAid(costProject, revenu, nbrOfPerson)
      .subscribe(result => {
        expect(result).toBe(expectedAmount);
      });
  });
});
