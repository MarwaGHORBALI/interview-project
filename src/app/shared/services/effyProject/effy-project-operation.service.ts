import { Injectable } from '@angular/core';
import { Observable, flatMap, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EffyProjectOperationService {

  constructor() { }

  costProject(surface: number): Observable<number> {
    return of(surface * 80)
  }

  amountOfAid(costProject: number, revenu: number, nbrOfPerson: number): Observable<number> {
    return of(costProject * 0.75 - (revenu / nbrOfPerson) * 0.15);
  }
}
