export class ProjectInformation {
    firstName!: string;
    lastName!: string;
    civility!: string;
    Email !: string;
    tel !: string;
    surface !: number;
    personalNbr !: number;
    situation !: string;
    revenu !: string;

}

export interface NextSteps {
    nextStepName: string, subStepName: string, displayNextStep: boolean 
}

export enum Steps {
    STEPONE = "STEPONE",
    STEPTWO = "STEPTWO",
    STEPTHREE = "STEPTHREE",
    NONELEGIBLE = "NONELEGIBLE"
}
