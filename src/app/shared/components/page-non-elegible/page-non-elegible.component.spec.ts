import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageNonElegibleComponent } from './page-non-elegible.component';
import { PrimeNgModule } from '@components/primeng.module';

describe('PageNonElegibleComponent', () => {
  let component: PageNonElegibleComponent;
  let fixture: ComponentFixture<PageNonElegibleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageNonElegibleComponent ],
      imports:[PrimeNgModule]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PageNonElegibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
