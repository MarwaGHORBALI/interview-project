import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NextSteps, Steps } from "@shared/models/project-information"
@Component({
  selector: 'shared-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() btnUsed !: string;
  @Input() formValidate !: boolean;
  @Input() stepName !: string;
  @Input() subStepName !: string;
  @Output() displayStep: EventEmitter<NextSteps> = new EventEmitter<NextSteps>();



  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  nextSteps() {
    switch (this.stepName) {
      case Steps.STEPONE:
        if (this.subStepName == "STEPONE_1") {
          this.displayStep.emit({ nextStepName: Steps.STEPONE, subStepName: "STEPONE_2", displayNextStep: true });
        } else if (this.subStepName == "STEPONE_2") {
          this.displayStep.emit({ nextStepName: Steps.STEPTWO, subStepName: "STEPTWO_1", displayNextStep: true });
        }
        break;
      case Steps.STEPTWO:
        if (this.subStepName == "STEPTWO_1")
          this.displayStep.emit({ nextStepName: Steps.STEPTWO, subStepName: "STEPTWO_2", displayNextStep: true });
        else if (this.subStepName == "STEPTWO_2")
          this.displayStep.emit({ nextStepName: Steps.STEPTWO, subStepName: "STEPTWO_3", displayNextStep: true });
        else if (this.subStepName == "STEPTWO_3")
          this.displayStep.emit({ nextStepName: Steps.STEPTWO, subStepName: "STEPTWO_4", displayNextStep: true });
        else if (this.subStepName == Steps.NONELEGIBLE)
          this.router.navigateByUrl('/project-effy-non-eligible')
        else
          this.displayStep.emit({ nextStepName: Steps.STEPTHREE, subStepName: "", displayNextStep: true });
        break;
      case Steps.STEPTHREE:
        this.displayStep.emit({ nextStepName: Steps.STEPTHREE, subStepName: "", displayNextStep: true });
        break;
        default :
          console.info('error next Step')

    }
  }
  backSteps() {
    switch (this.stepName) {
      case Steps.STEPONE:
        if (this.subStepName == "STEPONE_2")
          this.displayStep.emit({ nextStepName: Steps.STEPONE, subStepName: "STEPONE_1", displayNextStep: false });
        break;
      case Steps.STEPTWO:
        if (this.subStepName == "STEPTWO_1")
          this.displayStep.emit({ nextStepName: Steps.STEPONE, subStepName: "STEPONE_2", displayNextStep: false });
        else if (this.subStepName == "STEPTWO_2")
          this.displayStep.emit({ nextStepName: Steps.STEPTWO, subStepName: "STEPTWO_1", displayNextStep: false });
        else if (this.subStepName == "STEPTWO_3")
          this.displayStep.emit({ nextStepName: Steps.STEPTWO, subStepName: "STEPTWO_2", displayNextStep: false  });
          else if (this.subStepName == "STEPTWO_4")
          this.displayStep.emit({ nextStepName: Steps.STEPTWO, subStepName: "STEPTWO_3", displayNextStep: false  });
          break;
          default :
          console.info('error back step')
    }
  }
}

