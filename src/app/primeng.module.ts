import { NgModule } from "@angular/core";
import { CardModule } from "primeng/card";
import { RadioButtonModule } from "primeng/radiobutton";
import { AvatarModule } from "primeng/avatar";
import { KeyFilterModule } from "primeng/keyfilter";
import { InputTextModule } from "primeng/inputtext";
import { FileUploadModule } from "primeng/fileupload";
import { DialogModule } from "primeng/dialog";
import { InputTextareaModule } from "primeng/inputtextarea";
import { CalendarModule } from "primeng/calendar";
import { InplaceModule } from "primeng/inplace";
import { PanelModule } from "primeng/panel";
import { ToastModule } from "primeng/toast";
import { ButtonModule } from "primeng/button";
import { MenuModule } from "primeng/menu";
import { CheckboxModule } from "primeng/checkbox";
import { StepsModule } from "primeng/steps";
import { PaginatorModule } from "primeng/paginator";
import { MessagesModule } from "primeng/messages";
import { TimelineModule } from "primeng/timeline";
import { AccordionModule } from "primeng/accordion";
import { BadgeModule } from "primeng/badge";
import { DropdownModule } from "primeng/dropdown";
import { SlideMenuModule } from "primeng/slidemenu";
import { ScrollTopModule } from "primeng/scrolltop";
import { ScrollPanelModule } from "primeng/scrollpanel";
import { ProgressSpinnerModule } from "primeng/progressspinner";
import { SplitButtonModule } from "primeng/splitbutton";
import { TreeTableModule } from "primeng/treetable";
import { MultiSelectModule } from "primeng/multiselect";
import { ContextMenuModule } from "primeng/contextmenu";
import {TabViewModule} from 'primeng/tabview';
import {ToolbarModule} from 'primeng/toolbar';


@NgModule({
  exports: [
    CardModule,
    RadioButtonModule,
    AvatarModule,
    KeyFilterModule,
    InputTextModule,
    FileUploadModule,
    DialogModule,
    InputTextareaModule,
    CalendarModule,
    InplaceModule,
    PanelModule,
    ToastModule,
    ButtonModule,
    MenuModule,
    CheckboxModule,
    StepsModule,
    PaginatorModule,
    MessagesModule,
    TimelineModule,
    AccordionModule,
    BadgeModule,
    DropdownModule,
    SlideMenuModule,
    ScrollTopModule,
    ScrollPanelModule,
    ProgressSpinnerModule,
    SplitButtonModule,
    TreeTableModule,
    MultiSelectModule,
    ContextMenuModule,
    TabViewModule,
    ToolbarModule
  ],
})
export class PrimeNgModule {}
