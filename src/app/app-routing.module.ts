import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppLayoutComponent } from "@components/app-layout/app-layout.component";
import { PageNonElegibleComponent } from "@shared/components/page-non-elegible/page-non-elegible.component";



const routes: Routes = [
  {
    path: "",
    component: AppLayoutComponent,

    children: [
      {
        path: "",
        redirectTo: "/effy-travaux-aides",
        pathMatch: "full",
      },
      {
        path: "effy-travaux-aides",
        loadChildren: () =>
          import("@components/effy-project/effy-project.module").then((m) => m.effyProjectModule),
      },
     
       {
          path: "project-effy-non-eligible",
          component: PageNonElegibleComponent,
        },
      { path: "**", redirectTo: "/effy-travaux-aides", pathMatch: "full" },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
